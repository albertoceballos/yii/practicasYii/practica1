<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Autores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="autores-index">

    <h1><?= Html::encode($this->title) ?> <span class="glyphicon glyphicon-user" aria-hidden="true"></span></h1>
   
   <!-- <p> -->
       
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary'=>'Mostrando página {page} de {pageCount}.<br> Registros {begin} al {end}. Total de registros: {totalCount}',
        'tableOptions'=>['Class'=>'table table-striped table-bordered tabla1'],
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn'],
            
           
            'id',
            'autor',
             [
        'attribute' => 'foto',
        'format' => 'html',    
        'value' => function ($data) {
            return Html::img(Yii::getAlias('@web').'/imgs/'. $data['foto'],
                ['width' => '140px']);
        },
    ],
            

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    
  
    
</div>
